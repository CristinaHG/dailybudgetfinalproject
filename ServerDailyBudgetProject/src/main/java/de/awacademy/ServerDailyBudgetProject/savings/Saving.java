package de.awacademy.ServerDailyBudgetProject.savings;

import de.awacademy.ServerDailyBudgetProject.user.User;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
public class Saving {

    @javax.persistence.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    private User user;

    private BigDecimal amount;

    private LocalDateTime date;

    private Boolean monthly;

    private String name;

    public Saving() {
    }

    public Saving(User user, String name, BigDecimal amount, Boolean monthly) {
        this.user = user;
        this.name = name;
        this.amount = amount;
        this.monthly = monthly;
        this.date = LocalDateTime.now();
    }

    public long getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public Boolean getMonthly() {
        return monthly;
    }

    public String getName() {
        return name;
    }

    public void setId(long id) {
        id = id;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public void setMonthly(Boolean monthly) {
        this.monthly = monthly;
    }

    public void setName(String name) {
        this.name = name;
    }
}
