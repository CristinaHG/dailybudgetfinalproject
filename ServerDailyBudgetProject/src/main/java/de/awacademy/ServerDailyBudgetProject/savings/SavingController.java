package de.awacademy.ServerDailyBudgetProject.savings;

import de.awacademy.ServerDailyBudgetProject.auth.security.SecurityService;
import de.awacademy.ServerDailyBudgetProject.expense.ExpenseDTO;
import de.awacademy.ServerDailyBudgetProject.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@RestController
public class SavingController {

    @Autowired
    private SavingRepository savingRepository;

    @Autowired
    SecurityService securityService;

    @GetMapping("/api/savingHistory")
    public List<SavingDTO> showSavings() {

        Optional<User> optionalUser = securityService.getCurrentUser();

        List<SavingDTO> allBySavingName = new LinkedList<>();

        if(optionalUser.isPresent()) {

            Optional<List<Saving>> optionalSavingListPerUser = savingRepository.findAllByUserOrderByDateDesc(optionalUser);
            if(optionalSavingListPerUser.isPresent()){

                for (Saving saving : optionalSavingListPerUser.get()) {
                    allBySavingName.add(new SavingDTO(saving.getName(),saving.getAmount(), saving.getDate(), saving.getId()));
                }
            }
        }
        return allBySavingName;
    }

    @GetMapping("/api/editSaving")
    public Saving edit (@RequestParam long id) {
        Optional<User> optionalUser = securityService.getCurrentUser();

        return savingRepository.findByIdAndUser(id, optionalUser);
    }

    @PostMapping("/api/editSaving")
    public void editExpense(@RequestBody SavingDTO savingDTO) {
        Optional<User> optionalUser = securityService.getCurrentUser();

        Saving saving = savingRepository.findByIdAndUser(savingDTO.getId(), optionalUser);
        saving.setName(savingDTO.getName());
        saving.setAmount(savingDTO.getAmount());
        savingRepository.save(saving);
    }


    @PostMapping("/api/saving")
    public List<SavingDTO> addNewSaving(@RequestBody SavingDTO savingDTO){

        Optional<User> optionalUser = securityService.getCurrentUser();

        Saving saving = new Saving(optionalUser.get(), savingDTO.getName(), savingDTO.getAmount(), savingDTO.getMonthly());
        savingRepository.save(saving);

        return showSavings();
    }

    @PostMapping("/api/deleteSaving")
    public void deleteIncome(@RequestBody long id) {
        Optional<User> optionalUser = securityService.getCurrentUser();

        Saving saving = savingRepository.findById(id);

        if(optionalUser.isPresent()) {

            savingRepository.delete(saving);

        }

    }

}
