package de.awacademy.ServerDailyBudgetProject.savings;

import de.awacademy.ServerDailyBudgetProject.income.Income;
import de.awacademy.ServerDailyBudgetProject.user.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface SavingRepository extends JpaRepository<Saving, Long> {

//    List<Saving> findAllByUser(Optional<User> user);

    List <Saving> findAllByDateBetweenAndUser(
            LocalDateTime localDateStart,
            LocalDateTime localDateEnd,
            Optional<User> currentUser);

    Optional<List<Saving>> findAllByUserOrderByDateDesc(Optional<User> currentUser);

    Saving findById(long id);

    Saving findByIdAndUser (long id, Optional<User> currentUser);

    List <Saving> findAllByNameAndUser (String name, Optional<User> currentUser);

}
