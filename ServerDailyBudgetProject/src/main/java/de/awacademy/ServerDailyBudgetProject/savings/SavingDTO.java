package de.awacademy.ServerDailyBudgetProject.savings;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class SavingDTO {

    private BigDecimal amount;
    private Boolean monthly;
    private LocalDateTime date;
    private long id;
    private String name;

    public SavingDTO() {
    }

    public SavingDTO(BigDecimal amount) {
        this.amount = amount;
    }

    public SavingDTO(String name, BigDecimal amount, LocalDateTime date, long id) {
        this.name = name;
        this.amount = amount;
        this.date = date;
        this.id = id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Boolean getMonthly() {
        return monthly;
    }

    public void setMonthly(Boolean monthly) {
        this.monthly = monthly;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }
}
