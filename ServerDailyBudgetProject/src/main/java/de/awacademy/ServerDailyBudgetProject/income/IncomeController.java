package de.awacademy.ServerDailyBudgetProject.income;

import de.awacademy.ServerDailyBudgetProject.auth.security.SecurityService;
import de.awacademy.ServerDailyBudgetProject.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@RestController
public class IncomeController {

    @Autowired
    private IncomeRepository incomeRepository;

    @Autowired
    SecurityService securityService;

    @GetMapping("/api/incomeHistory")
    public List<IncomeDTO> showIncome() {

        Optional<User> optionalUser = securityService.getCurrentUser();

        List<IncomeDTO> allByIncomeName = new LinkedList<>();

        if(optionalUser.isPresent()) {

            Optional<List<Income>> optionalIncomeListPerUser = incomeRepository.findAllByUserOrderByDateDesc(optionalUser);
            if(optionalIncomeListPerUser.isPresent()){

                for (Income income : optionalIncomeListPerUser.get()) {
                    allByIncomeName.add(new IncomeDTO(income.getName(), income.getAmount(), income.getDate(), income.getId()));
                }
            }
        }
        return allByIncomeName;
    }

    @GetMapping("/api/editIncome")
    public Income edit (@RequestParam long id) {
        Optional<User> optionalUser = securityService.getCurrentUser();

        return incomeRepository.findByIdAndUser(id, optionalUser);
    }

    @PostMapping("/api/editIncome")
    public void editIncome(@RequestBody IncomeDTO incomeDTO) {
        Optional<User> optionalUser = securityService.getCurrentUser();

        Income income = incomeRepository.findByIdAndUser(incomeDTO.getId(), optionalUser);
        income.setName(incomeDTO.getName());
        income.setAmount(incomeDTO.getAmount());
        incomeRepository.save(income);
    }

   @PostMapping("/api/income")
   public List<IncomeDTO> addNewIncome(@RequestBody IncomeDTO incomeDTO){

       Optional<User> optionalUser = securityService.getCurrentUser();

//       if (optionalUser.isPresent()){
           Income income = new Income(optionalUser.get(), incomeDTO.getName(), incomeDTO.getAmount(), incomeDTO.getMonthly());
           incomeRepository.save(income);
           return showIncome();
//       }
   }

    @PostMapping("/api/deleteIncome")
    public void deleteIncome(@RequestBody long id) {
        Optional<User> optionalUser = securityService.getCurrentUser();

        Income income = incomeRepository.findById(id);

        if(optionalUser.isPresent()) {

            incomeRepository.delete(income);

        }

    }


}
