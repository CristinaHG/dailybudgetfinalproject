package de.awacademy.ServerDailyBudgetProject.income;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class IncomeDTO {

    private String name;
    private BigDecimal amount;
    private Boolean monthly;
    private long id;
    private LocalDateTime date;



    public IncomeDTO() {
    }

    public IncomeDTO(String Name, BigDecimal amount, LocalDateTime date, long id) {
        this.name = Name;
        this.amount = amount;
        this.date = date;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Boolean getMonthly() {
        return monthly;
    }

    public void setMonthly(Boolean monthly) {
        this.monthly = monthly;
    }

    public LocalDateTime getDate() {
        return date;
    }


    public long getId() {
        return id;
    }
}
