package de.awacademy.ServerDailyBudgetProject.income;

import de.awacademy.ServerDailyBudgetProject.user.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface IncomeRepository extends JpaRepository<Income, Long> {

    List<Income> findAll();

    List<Income> findAllByMonthlyAndUser(Boolean monthly, Optional<User> currentUser);

    Optional<List<Income>> findAllByDateBetweenAndMonthlyAndUser(
            LocalDateTime localDateStart,
            LocalDateTime localDateEnd,
            boolean monthly,
            Optional<User> currentUser);

    List<Income> findAllByDateBetweenAndUser(
            LocalDateTime localDateStart,
            LocalDateTime localDateEnd,
            Optional<User> currentUser);

    Optional<List<Income>> findAllByUserOrderByDateDesc(Optional<User> currentUser);

    Income findByIdAndUser(long id, Optional<User> currentUser);

    Income findById(long id);


}


