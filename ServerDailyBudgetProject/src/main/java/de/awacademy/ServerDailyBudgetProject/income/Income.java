package de.awacademy.ServerDailyBudgetProject.income;

import de.awacademy.ServerDailyBudgetProject.user.User;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
public class Income {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    private User user;

    private String name;

    private BigDecimal amount;

    private LocalDateTime date;

    private Boolean monthly;

    public Income() {
    }

    public Income(User user, String name, BigDecimal amount, Boolean monthly) {
        this.user = user;
        this.name = name;
        this.amount = amount;
        this.date = LocalDateTime.now();
        this.monthly = monthly;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public Boolean getMonthly() {
        return monthly;
    }

    public void setMonthly(Boolean monthly) {
        this.monthly = monthly;
    }

    public long getId() {
        return id;
    }
}
