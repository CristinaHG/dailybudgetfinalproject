package de.awacademy.ServerDailyBudgetProject.expense;

import de.awacademy.ServerDailyBudgetProject.auth.security.SecurityService;
import de.awacademy.ServerDailyBudgetProject.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@RestController
public class ExpenseController {

    @Autowired
    private ExpenseRepository expenseRepository;

    @Autowired
    private SecurityService securityService;

    @GetMapping("/api/expenseHistory")
    public List<ExpenseDTO> showExpenses() {

        Optional<User> optionalUser = securityService.getCurrentUser();

        List<ExpenseDTO> allByExpenseName = new LinkedList<>();

        if(optionalUser.isPresent()) {

            Optional<List<Expense>> optionalExpenseListPerUser = expenseRepository.findAllByUserOrderByDateDesc(optionalUser);
            if(optionalExpenseListPerUser.isPresent()){

                for (Expense expense : optionalExpenseListPerUser.get()) {
                    allByExpenseName.add(new ExpenseDTO(expense.getName(), expense.getAmount(), expense.getDate(), expense.getId()));
                }
            }
        }
        return allByExpenseName;
    }


    @GetMapping("/api/editExpense")
    public Expense edit (@RequestParam long id) {
        Optional<User> optionalUser = securityService.getCurrentUser();

        return expenseRepository.findByIdAndUser(id, optionalUser);
    }

    @PostMapping("/api/editExpense")
    public void editExpense(@RequestBody ExpenseDTO expenseDTO) {
        Optional<User> optionalUser = securityService.getCurrentUser();

        Expense expense = expenseRepository.findByIdAndUser(expenseDTO.getId(), optionalUser);
        expense.setName(expenseDTO.getName());
        expense.setAmount(expenseDTO.getAmount());
        expenseRepository.save(expense);
    }

    @PostMapping("/api/expense")
    public List<ExpenseDTO> addNewExpense(@RequestBody ExpenseDTO expenseDTO) {
        Optional<User> optionalUser = securityService.getCurrentUser();

        Expense expense = new Expense(optionalUser.get(), expenseDTO.getName(), expenseDTO.getAmount(), expenseDTO.getMonthly());
        expenseRepository.save(expense);
        return showExpenses();
    }

    @PostMapping("/api/deleteExpense")
    public void deleteExpense(@RequestBody long id) {
        Optional<User> optionalUser = securityService.getCurrentUser();

        Expense expense = expenseRepository.findById(id);

        if(optionalUser.isPresent()) {

            expenseRepository.delete(expense);

        }

    }


}