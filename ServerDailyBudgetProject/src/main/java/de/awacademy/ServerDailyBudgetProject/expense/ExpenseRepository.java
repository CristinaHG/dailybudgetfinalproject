package de.awacademy.ServerDailyBudgetProject.expense;

import de.awacademy.ServerDailyBudgetProject.user.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface ExpenseRepository extends JpaRepository<Expense, Long> {

    List<Expense> findAll();

    List<Expense> findAllByMonthlyAndUser(Boolean monthly, Optional<User> currentUser);

    Optional<List<Expense>> findAllByDateBetweenAndMonthlyAndUser(
            LocalDateTime localDateStart,
            LocalDateTime localDateEnd,
            boolean monthly,
            Optional<User> currentUser);

    List<Expense> findAllByDateBetweenAndUser(
            LocalDateTime localDateStart,
            LocalDateTime localDateEnd,
            Optional<User> currentUser);

    Optional<List<Expense>> findAllByUserOrderByDateDesc(Optional<User> currentUser);

    Expense findByIdAndUser(long id, Optional<User> currentUser);

    Expense findById(long id);


}



