package de.awacademy.ServerDailyBudgetProject.expense;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class ExpenseDTO {

    private String name;
    public BigDecimal amount;
    private Boolean monthly;
    private long id;
    private LocalDateTime date;

    public ExpenseDTO() {

    }

    public String getName() {
        return name;
    }

    public ExpenseDTO(String name, BigDecimal amount, LocalDateTime date, long id) {
        this.name = name;
        this.amount = amount;
        this.date = date;
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;

    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Boolean getMonthly() {
        return monthly;
    }

    public void setMonthlyExpense(Boolean monthly) {
        this.monthly = monthly;
    }

    public long getId() {
        return id;
    }

    public LocalDateTime getDate() {
        return date;
    }

}
