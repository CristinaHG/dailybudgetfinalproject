package de.awacademy.ServerDailyBudgetProject;

import de.awacademy.ServerDailyBudgetProject.auth.security.SecurityConfiguration;
import de.awacademy.ServerDailyBudgetProject.user.User;
import de.awacademy.ServerDailyBudgetProject.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.security.crypto.password.PasswordEncoder;


@SpringBootApplication
@Import(SecurityConfiguration.class)
public class ServerDailyBudgetProjectApplication implements CommandLineRunner {

	@Autowired
	private UserRepository userRepository;
	@Autowired
	private PasswordEncoder passwordEncoder;

	public static void main(String[] args) {
		SpringApplication.run(ServerDailyBudgetProjectApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
	}
}
