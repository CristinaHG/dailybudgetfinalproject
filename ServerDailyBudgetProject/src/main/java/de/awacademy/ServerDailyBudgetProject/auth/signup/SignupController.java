package de.awacademy.ServerDailyBudgetProject.auth.signup;

import de.awacademy.ServerDailyBudgetProject.user.User;
import de.awacademy.ServerDailyBudgetProject.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
public class SignupController {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserRepository userRepository;

    @PostMapping("/api/signup")
    public boolean signup(@RequestBody SignupDTO signupDTO, BindingResult bindingResult) {

        boolean signUpSuccessful = false;

        Optional<User> optionalUser = userRepository.findFirstByUsername(signupDTO.getUsername());

        if (optionalUser.isPresent()){
            return signUpSuccessful;
        } else {
            User user = new User(signupDTO.getUsername(), passwordEncoder.encode(signupDTO.getPassword()));
            userRepository.save(user);

            return signUpSuccessful = true;
        }
    }

}
