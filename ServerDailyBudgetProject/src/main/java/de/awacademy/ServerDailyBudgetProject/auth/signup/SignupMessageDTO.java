package de.awacademy.ServerDailyBudgetProject.auth.signup;

public class SignupMessageDTO {

    String text = "";

    public SignupMessageDTO() {
    }

    public SignupMessageDTO(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}
