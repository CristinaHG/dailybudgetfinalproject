package de.awacademy.ServerDailyBudgetProject.auth.login;

import de.awacademy.ServerDailyBudgetProject.auth.security.SecurityService;
import de.awacademy.ServerDailyBudgetProject.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Optional;

@RestController
public class LoginController {
    @Autowired
    SecurityService securityService;

    @GetMapping("/api/login")
    public HashMap<String, String> test() {
        Optional<User> optionalUser = securityService.getCurrentUser();

        HashMap<String, String> ret = new HashMap<>();
        if(optionalUser.isPresent()) {
            ret.put("hello", optionalUser.get().getUsername());
        }
        else {
            ret.put("hello", "World");
        }
        return ret;
    }



}
