package de.awacademy.ServerDailyBudgetProject.auth.security;

import de.awacademy.ServerDailyBudgetProject.user.User;
import de.awacademy.ServerDailyBudgetProject.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class SecurityService {
    @Autowired
    private UserRepository userRepository;

    public Optional<User> getCurrentUser() {
        Object principal = SecurityContextHolder.getContext()
                .getAuthentication().getPrincipal();

        if(principal instanceof SecurityUserDetails) {
            return userRepository.findFirstByUsername(((SecurityUserDetails) principal).getUsername());
        }
        return Optional.empty();
    }

}
