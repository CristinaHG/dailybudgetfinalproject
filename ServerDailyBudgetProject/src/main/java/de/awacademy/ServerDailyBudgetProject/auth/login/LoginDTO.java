package de.awacademy.ServerDailyBudgetProject.auth.login;

public class LoginDTO {
    private String username;

    public LoginDTO(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
