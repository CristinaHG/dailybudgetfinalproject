package de.awacademy.ServerDailyBudgetProject.budget;

import java.math.BigDecimal;

public class Budget {

    private BigDecimal total;

    private int daysUntilMonthEnd;

    private BigDecimal currentDailyBudget;

    private BigDecimal saving;

    public Budget() {
    }


    public Budget(
            BigDecimal total, int daysUntilMonthEnd, BigDecimal currentDailyBudget, BigDecimal saving) {
        this.total = total;
        this.daysUntilMonthEnd = daysUntilMonthEnd;
        this.currentDailyBudget = currentDailyBudget;
        this.saving = saving;
    }



    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public int getDaysUntilMonthEnd() {
        return daysUntilMonthEnd;
    }

    public void setDaysUntilMonthEnd(int daysUntilMonthEnd) {
        this.daysUntilMonthEnd = daysUntilMonthEnd;
    }

    public BigDecimal getCurrentDailyBudget() {
        return currentDailyBudget;
    }

    public void setCurrentDailyBudget(BigDecimal currentDailyBudget) {
        this.currentDailyBudget = currentDailyBudget;
    }

    public BigDecimal getSaving() {
        return saving;
    }

    public void setSaving(BigDecimal saving) {
        this.saving = saving;
    }
}
