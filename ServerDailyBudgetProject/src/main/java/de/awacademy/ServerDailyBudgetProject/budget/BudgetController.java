package de.awacademy.ServerDailyBudgetProject.budget;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BudgetController {

    @Autowired
    BudgetService budgetService;

    @GetMapping("/api/budget")
    public Budget getBudgetMonthly(){

        return budgetService.createBudgetPackage();

    }

}
