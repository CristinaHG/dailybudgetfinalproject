package de.awacademy.ServerDailyBudgetProject.budget;

import de.awacademy.ServerDailyBudgetProject.auth.security.SecurityService;
import de.awacademy.ServerDailyBudgetProject.expense.Expense;
import de.awacademy.ServerDailyBudgetProject.expense.ExpenseRepository;
import de.awacademy.ServerDailyBudgetProject.income.Income;
import de.awacademy.ServerDailyBudgetProject.income.IncomeRepository;
import de.awacademy.ServerDailyBudgetProject.savings.Saving;
import de.awacademy.ServerDailyBudgetProject.savings.SavingRepository;
import de.awacademy.ServerDailyBudgetProject.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jmx.export.naming.IdentityNamingStrategy;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.TemporalAdjusters;
import java.util.List;
import java.util.Optional;

@Service
public class BudgetService {

    @Autowired
    private ExpenseRepository expenseRepository;

    @Autowired
    private IncomeRepository incomeRepository;

    @Autowired
    private SavingRepository savingRepository;

    @Autowired
    SecurityService securityService;

    /**
     * Creates a Budget object to send to the client with all
     * the necessary information about the budget
     * @return
     * total = Remaining budget after incomes and expenses are added
     * daysUntilMonthEnd = remaining days to the end of the month, counting from today (LocalDateTime.now())
     * dailyBudget = updated daily budget
     * currentDailyBudget = budget for today
     * saving = Amount of savings (monthly and not monthly).
     */
    public Budget createBudgetPackage(){

        BigDecimal total = budgetAll();

        int daysUntilMonthEnd = daysUntilMonthEnd();

        BigDecimal currentDailyBudget = calculateCurrentDailyBudget();

        BigDecimal saving = sumSaving();

        return new Budget(total, daysUntilMonthEnd, currentDailyBudget, saving);
    }

    public BigDecimal calculateCurrentDailyBudget(){

        BigDecimal daysUntilMonthEnd = new BigDecimal(daysUntilMonthEnd());

        BigDecimal currentBudget = budgetAll()
                .add(calculateExpensesOnlyToday())
                .subtract(calculateIncomesOnlyToday())
                .subtract(calculateExpensesOnlyToday())
                .add(calculateIncomesOnlyToday());

        if(daysUntilMonthEnd.intValue() > 0) {
            return currentBudget.divide(BigDecimal.valueOf(daysUntilMonthEnd()), 2, RoundingMode.HALF_UP);
        }

        return currentBudget;

    }

    public BigDecimal budgetAll(){

        return sumIncomeMonthly()
                .add(sumIncomeNOTMonthly())
                .subtract(sumExpenseMonthly())
                .subtract(sumExpenseNOTMonthly())
                .subtract(sumSaving());
    }

    public BigDecimal sumSaving(){

        Optional<User> optionalUser = securityService.getCurrentUser();

        Optional<List<Saving>> savingListI = savingRepository.findAllByUserOrderByDateDesc(optionalUser);

        LocalDateTime now = LocalDateTime.now();
        LocalDateTime monthStart = now.with(TemporalAdjusters.firstDayOfMonth()).with(LocalTime.MIN);
        LocalDateTime monthEnd = now.with(TemporalAdjusters.lastDayOfMonth()).with(LocalTime.MAX);

        List<Saving> currentSavings = savingRepository.findAllByDateBetweenAndUser(monthStart, monthEnd, optionalUser);
        int same = 0;

        if(savingListI.isPresent()){

            for(Saving saving : savingListI.get()){

                if(currentSavings.isEmpty()){
                    Saving saving1 = new Saving(optionalUser.get(), saving.getName(), saving.getAmount(), true);
                    savingRepository.save(saving1);
                    same = 1;
                }else {
                    for (Saving savingII : currentSavings) {
                        if (savingII.getName().equals(saving.getName())) {

                            same = same + 1;
                        }
                    }
                }

                if(same == 0){
                    Saving saving1 = new Saving(optionalUser.get(), saving.getName(), saving.getAmount(), true);
                    savingRepository.save(saving1);
                    same = 0;
                }

            }

        }

        List<Saving> savingList = savingRepository.findAllByUserOrderByDateDesc(optionalUser).get();

        return sumOfSavingBigDecimals(savingList);
    }

//    public BigDecimal sumSavingOfTheMonth(){
//        Optional<User> optionalUser = securityService.getCurrentUser();
//
//        BigDecimal bdSum = BigDecimal.valueOf(0);
//
//        LocalDateTime now = LocalDateTime.now();
//        LocalDateTime monthStart = now.with(TemporalAdjusters.firstDayOfMonth()).with(LocalTime.MIN);
//        LocalDateTime monthEnd = now.with(TemporalAdjusters.lastDayOfMonth()).with(LocalTime.MAX);
//
//        Optional<List<Saving>> optionalSavingList = savingRepository.findAllByDateBetweenAndUser(monthStart, monthEnd, optionalUser);
//
//        if(optionalSavingList.isPresent()){
//            bdSum = sumOfSavingBigDecimals(optionalSavingList.get());
//        }
//
//        return bdSum;
//    }

    public BigDecimal sumIncomeMonthly (){
        Optional<User> optionalUser = securityService.getCurrentUser();

        List<Income> incomeListI = incomeRepository.findAllByMonthlyAndUser(true, optionalUser);

        LocalDateTime now = LocalDateTime.now();
        LocalDateTime monthStart = now.with(TemporalAdjusters.firstDayOfMonth()).with(LocalTime.MIN);
        LocalDateTime monthEnd = now.with(TemporalAdjusters.lastDayOfMonth()).with(LocalTime.MAX);

        List<Income> currentMonthIncomes = incomeRepository.findAllByDateBetweenAndUser(monthStart, monthEnd, optionalUser);

        int same = 0;

        if(!incomeListI.isEmpty()) {

            for (Income income : incomeListI) {

                if (currentMonthIncomes.isEmpty()) {
                    Income income1 = new Income(optionalUser.get(), income.getName(), income.getAmount(), true);
                    incomeRepository.save(income1);
                    same = 1;
                } else {
                    for (Income incomeII : currentMonthIncomes) {
                        if (incomeII.getName().equals(income.getName())) {

                            same = same + 1;
                        }
                    }
                }

                if (same == 0) {

                    Income income1 = new Income(optionalUser.get(), income.getName(), income.getAmount(), true);
                    incomeRepository.save(income1);
                    same = 0;
                }

            }
        }
        List<Income> incomeList = incomeRepository.findAllByMonthlyAndUser(true, optionalUser);

        return sumOfIncomeBigDecimals (incomeList);
    }

//    public BigDecimal sumIncomeMonthly(Boolean monthly){
//
//        Optional<User> optionalUser = securityService.getCurrentUser();
//
//        List<Income> incomeList = incomeRepository.findAllByMonthlyAndUser(monthly, optionalUser);
//
//        return sumOfIncomeBigDecimals (incomeList);
//    }

    public BigDecimal sumExpenseMonthly (){
        Optional<User> optionalUser = securityService.getCurrentUser();

        List<Expense> expenseListI = expenseRepository.findAllByMonthlyAndUser(true, optionalUser);

        LocalDateTime now = LocalDateTime.now();
        LocalDateTime monthStart = now.with(TemporalAdjusters.firstDayOfMonth()).with(LocalTime.MIN);
        LocalDateTime monthEnd = now.with(TemporalAdjusters.lastDayOfMonth()).with(LocalTime.MAX);

        List<Expense> currentMonthExpense = expenseRepository.findAllByDateBetweenAndUser(monthStart, monthEnd, optionalUser);

        int same = 0;

        if(!expenseListI.isEmpty()) {

            for (Expense expense : expenseListI) {

                if (currentMonthExpense.isEmpty()) {
                    Expense expense1 = new Expense(optionalUser.get(), expense.getName(), expense.getAmount(), true);
                    expenseRepository.save(expense1);
                    same = 1;
                } else {
                    for (Expense expenseII : currentMonthExpense) {
                        if (expenseII.getName().equals(expense.getName())) {
                            same = same + 1;
                        }
                    }
                }

                if (same == 0) {
                    Expense expense1 = new Expense(optionalUser.get(), expense.getName(), expense.getAmount(), true);
                    expenseRepository.save(expense1);
                    same = 0;
                }

            }
        }
        List<Expense> expenseList = expenseRepository.findAllByMonthlyAndUser(true, optionalUser);

        return sumOfExpenseBigDecimals(expenseList);
    }

//    public BigDecimal sumExpenseMonthly(Boolean monthly){
//
//        Optional<User> optionalUser = securityService.getCurrentUser();
//
//        List<Expense> expenseList = expenseRepository.findAllByMonthlyAndUser(monthly, optionalUser);
//
//        return sumOfExpenseBigDecimals(expenseList);
//    }


    public BigDecimal sumExpenseNOTMonthly(){

        Optional<User> optionalUser = securityService.getCurrentUser();

        List<Expense> expenseList = expenseRepository.findAllByMonthlyAndUser(false, optionalUser);
        return sumOfExpenseBigDecimals(expenseList);
    }

    public BigDecimal sumIncomeNOTMonthly(){

        Optional<User> optionalUser = securityService.getCurrentUser();
        List<Income> incomeList = incomeRepository.findAllByMonthlyAndUser(false, optionalUser);

        return sumOfIncomeBigDecimals(incomeList);
    }


    /**
     * Get the amount of remaining days for the present month counting from today (LocalDateTime.now())
     * @return int remaining days
     */
    public int daysUntilMonthEnd (){

        LocalDate date = LocalDate.now();
        int daysInMonth = date.lengthOfMonth();
        int daysUntilMonthEnd = daysInMonth - date.getDayOfMonth();

        return daysUntilMonthEnd;

        /*The following uncomment lines are the test for the case "Last day of teh month" = 0 daysUntilMonthEnd
        * */

//        LocalDate dateTest = LocalDate.of(2019,01,31);
//        int daysInMonthTest = dateTest.lengthOfMonth();
//        int daysUntilMonthEndTest = daysInMonthTest - dateTest.getDayOfMonth();
//
//        return daysUntilMonthEndTest;

    }


    /**
     * Get all expense entries for today (LocalDateTime.now()) for current user
     * and add the corresponding amounts
     * @return BigDecimal sum value
     */
    public BigDecimal calculateExpensesOnlyToday(){

        Optional<User> optionalUser = securityService.getCurrentUser();

        BigDecimal bdSum = BigDecimal.valueOf(0);

        LocalDateTime now = LocalDateTime.now();
        LocalDateTime nowStart = now.with(LocalTime.MIN);
        LocalDateTime nowEnd = now.with(LocalTime.MAX);

        Optional<List<Expense>> optionalExpenseList = expenseRepository.findAllByDateBetweenAndMonthlyAndUser(nowStart, nowEnd, false, optionalUser);

        if(optionalExpenseList.isPresent()){
            bdSum = sumOfExpenseBigDecimals(optionalExpenseList.get());
        }
        return bdSum;
    }

    /**
     * Get all income entries for today (LocalDateTime.now()) for current user
     * and add the corresponding amounts
     * @return BigDecimal sum value
     */
    public BigDecimal calculateIncomesOnlyToday(){

        Optional<User> optionalUser = securityService.getCurrentUser();

        BigDecimal bdSum = BigDecimal.valueOf(0);

        LocalDateTime now = LocalDateTime.now();
        LocalDateTime nowStart = now.with(LocalTime.MIN);
        LocalDateTime nowEnd = now.with(LocalTime.MAX);

        Optional<List<Income>> optionalIncomeList =
                incomeRepository.findAllByDateBetweenAndMonthlyAndUser(nowStart, nowEnd, false, optionalUser);

        if(optionalIncomeList.isPresent()){
            bdSum = sumOfIncomeBigDecimals(optionalIncomeList.get());
        }
        return bdSum;
    }


    /**
     * Add BigDecimal amount attribute of a list of Expense objects
     * @param expenseList
     * @return BigDecimal sum value
     */
    public BigDecimal sumOfExpenseBigDecimals(List<Expense> expenseList){

        BigDecimal bdSum = BigDecimal.valueOf(0);

        for(Expense item : expenseList){
            bdSum = bdSum.add(item.getAmount());
        }
        return bdSum;
    }

    /**
     * Add BigDecimal amount attribute of a list of Income objects
     * @param incomeList
     * @return BigDecimal sum value
     */
    public BigDecimal sumOfIncomeBigDecimals(List<Income> incomeList){

        BigDecimal bdSum = BigDecimal.valueOf(0);

        for(Income item : incomeList){
            bdSum = bdSum.add(item.getAmount());
        }
        return bdSum;
    }

    /**
     * Add BigDecimal amount attribute of a list of Saving objects
     * @param savingList
     * @return BigDecimal sum value
     */
    public BigDecimal sumOfSavingBigDecimals(List<Saving> savingList){

        BigDecimal bdSum = BigDecimal.valueOf(0);

        for(Saving item : savingList){
            bdSum = bdSum.add(item.getAmount());
        }
        return bdSum;
    }



}