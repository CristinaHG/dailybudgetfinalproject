package de.awacademy.ServerDailyBudgetProject.budget;

import de.awacademy.ServerDailyBudgetProject.expense.Expense;
import de.awacademy.ServerDailyBudgetProject.expense.ExpenseRepository;
import de.awacademy.ServerDailyBudgetProject.income.Income;
import de.awacademy.ServerDailyBudgetProject.income.IncomeRepository;
import de.awacademy.ServerDailyBudgetProject.user.User;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.LinkedList;
import java.util.List;

public class BudgetServiceTest {

    @Mock
    private ExpenseRepository expenseRepository;

    @Mock
    private IncomeRepository incomeRepository;

    @InjectMocks
    private BudgetService budgetService;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void sumOfIncomeBigDecimals(){

        User user = new User("user", "user");
        Income income1 = new Income(user, "income", BigDecimal.valueOf(10), false);
        Income income2 = new Income(user, "income", BigDecimal.valueOf(10), false);
        BigDecimal expected = BigDecimal.valueOf(20);

        List<Income> incomeList = new LinkedList<>();
        incomeList.add(income1);
        incomeList.add(income2);

        BigDecimal bdSum = budgetService.sumOfIncomeBigDecimals(incomeList);

        Assert.assertEquals(expected, bdSum);

    }

    @Test
    public void sumOfExpenseBigDecimals(){

        User user = new User("user", "user");
        Expense expense1 = new Expense(user, "expense", BigDecimal.valueOf(10), false);
        Expense expense2 = new Expense(user, "expense", BigDecimal.valueOf(10), false);
        BigDecimal expected = BigDecimal.valueOf(20);

        List<Expense> expenseList = new LinkedList<>();
        expenseList.add(expense1);
        expenseList.add(expense2);

        BigDecimal bdSum = budgetService.sumOfExpenseBigDecimals(expenseList);

        Assert.assertEquals(expected, bdSum);

    }

    @Test
    public void calculateIncomesOnlyToday(){

        LocalDateTime now = LocalDateTime.now();
        LocalDateTime nowStart = now.with(LocalTime.MIN);
        LocalDateTime nowEnd = now.with(LocalTime.MAX);

        User user = new User("user", "user");
        Income income1 = new Income(user, "income", BigDecimal.valueOf(10), false);
        Income income2 = new Income(user, "income", BigDecimal.valueOf(10), false);
        //income3 is not from today
        Income income3 = new Income(user, "income", BigDecimal.valueOf(10), false);
        income3.setDate(LocalDateTime.now().plusDays(4));

        BigDecimal expected = BigDecimal.valueOf(30);

        List<Income> incomeList = new LinkedList<>();
        incomeList.add(income1);
        incomeList.add(income2);
        incomeList.add(income3);

//        Mockito.when(
//                incomeRepository.findAllByIncomeDateBetweenAndMonthly(nowStart, nowEnd, false))
//                .thenReturn(Optional.of(incomeList));

        BigDecimal bdSum = budgetService.calculateIncomesOnlyToday();

        Assert.assertEquals(expected, bdSum);

    }


}
