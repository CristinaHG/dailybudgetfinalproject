import { Component, OnInit } from '@angular/core';
import {SecurityService} from '../services/security.service';
import {NgForm, Validators} from '@angular/forms';
import {User} from '../user';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  hide = true;

  user: User | null;

  currentUser = {
    username: '',
    password: ''
  };

  successfulSignupMessage = false;

  failedLoginMessage = false;

  constructor(private security: SecurityService, private router: Router) {
    this.security.getCurrentUser().subscribe(u => this.user = u);
  }

  onSubmit(form: NgForm) {
    console.log(form);
  }

  login() {
    this.security.login(this.currentUser.username, this.currentUser.password).subscribe(
      () => {
          this.router.navigateByUrl('budget')
        },
      () =>{
          this.failedLoginMessage = true;
          this.currentUser.username = '';
          this.currentUser.password = '';
        }
      );
  }

  logout() {
    this.security.logout();
  }


}
