import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router} from '@angular/router';
import {Observable} from "rxjs";
import {SecurityService} from "./security.service";
import {map} from "rxjs/operators";

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {

  constructor(private security: SecurityService, private router: Router){}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.security.getCurrentUser().pipe(
      map(u => {
        if(u===null) {
          this.router.navigateByUrl('login');
          return false;
        }
        return true;
      })
    );
  }

}








