import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {NgForm} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  newUser = {
    username: '',
    password: '',
    confirmPassword: '',
  };

  signupMessage: string;

  successfulSignupMessage: boolean;

  passwordsDoNotMatch: boolean;

  constructor(private http: HttpClient, private router: Router) {
  }

  ngOnInit() {
  }

  onSubmit(form: NgForm) {
    console.log(form);
  }

  createNewUser() {

    if(this.newUser.password !== this.newUser.confirmPassword){
      this.passwordsDoNotMatch = true;
      this.newUser.password = '';
      this.newUser.confirmPassword = '';
    } else {
      const requestObservable = this.http.post<boolean>('/api/signup', this.newUser);
      requestObservable.subscribe(result => {
        if (result) {
          this.successfulSignupMessage = true;
          this.router.navigateByUrl('login');
        } else {
          this.signupMessage = 'You are already registered';
          this.newUser.username = '';
          this.newUser.password = '';
          this.newUser.confirmPassword = '';
        }
      });
    }
  }


}
