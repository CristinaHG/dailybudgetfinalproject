import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {SignupComponent} from './auth/signup/signup.component';
import {LoginComponent} from './auth/login/login.component';
import {IncomeComponent} from './transaction/income/income.component';
import {ExpenseComponent} from './transaction/expense/expense.component';
import {BudgetComponent} from './budget/budget.component';
import {NotfoundComponent} from './notfound/notfound.component';
import {AuthGuard} from './auth/services/auth.guard';
import {SavingComponent} from './transaction/saving/saving.component';
import {HistoryComponent} from './history/history.component';
import {EditExpenseComponent} from './edit/edit-expense/edit-expense.component';
import {ExpenseHistoryComponent} from './history/expense-history/expense-history.component';
import {IncomeHistoryComponent} from './history/income-history/income-history.component';
import {SavingHistoryComponent} from './history/saving-history/saving-history.component';
import {EditIncomeComponent} from './edit/edit-income/edit-income.component';
import {EditSavingComponent} from './edit/edit-saving/edit-saving.component';


const routes: Routes = [
  {path: '', redirectTo: 'login', pathMatch: 'full'},
  {path: 'budget', component: BudgetComponent, data: {state: 'budget'}, canActivate: [AuthGuard]},
  {path: 'signup', component: SignupComponent, data: {state: 'signup'}},
  {path: 'login', component: LoginComponent, data: {state: 'login'}},
  {path: 'income', component: IncomeComponent, data: {state: 'income'}, canActivate: [AuthGuard]},
  {path: 'expense', component: ExpenseComponent, data: {state: 'expense'}, canActivate: [AuthGuard]},
  {path: 'saving', component: SavingComponent, data: {state: 'saving'}, canActivate: [AuthGuard]},
  {path: 'edit-expense/:id', component: EditExpenseComponent},
  {path: 'edit-income/:id', component: EditIncomeComponent},
  {path: 'edit-saving/:id', component: EditSavingComponent},
  {path: 'savingHistory', component: SavingHistoryComponent, data: {state: 'savingHistory'}, canActivate: [AuthGuard]},
  {path: 'income', component: IncomeComponent, data: {state: 'income'}},
  {path: 'expense', component: ExpenseComponent, data: {state: 'expense'}},
  {path: 'expenseHistory', component: ExpenseHistoryComponent, data: {state: 'expenseHistory'}, canActivate: [AuthGuard]},
  {path: 'incomeHistory', component: IncomeHistoryComponent, data: {state: 'incomeHistory'}, canActivate: [AuthGuard]},
  {path: 'history', component: HistoryComponent, data: {state: 'history'}, canActivate: [AuthGuard]},
  {path: '**', component: NotfoundComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {
}
