import { Component, OnInit } from '@angular/core';
import {User} from '../../auth/user';
import {Income} from '../../transaction/income/income';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {NgForm} from '@angular/forms';
import {Expense} from '../../transaction/expense/expense';
import {DeleteComponent} from "../../delete-expense";
import {MatDialog} from "@angular/material";

@Component({
  selector: 'app-income-history',
  templateUrl: './income-history.component.html',
  styleUrls: ['./income-history.component.css']
})
export class IncomeHistoryComponent implements OnInit {

  public user: User;

  incomes: Income[];

  selectedDate: any;

  maxDate;

  constructor(private http: HttpClient, private router: Router, private dialog: MatDialog) {
  }

  ngOnInit() {
    this.http.get<Income[]>('/api/incomeHistory').subscribe(
      i => this.incomes = i
    );
    this.maxDate = new Date();
    this.maxDate.setFullYear(this.maxDate.getFullYear());
    this.loadIncomes();
  }

  loadIncomes() {
    this.http.get<Income[]>('/api/incomeHistory').subscribe(
      i => this.incomes = i
    );
  }

  onSubmit(form: NgForm) {
    console.log(form);
  }

  updateIncomes() {
    this.selectedDate = undefined;
  }

  onDelete(id: number){
    const dialogRef = this.dialog.open(DeleteComponent)
    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.deleteIncome(id);
      } else {
        this.loadIncomes();
      }
    })
  }

  deleteIncome(id: number) {
    this.http.post<Expense>('/api/deleteIncome', id).subscribe(
      () => {
        this.loadIncomes();
      });
  }


}
