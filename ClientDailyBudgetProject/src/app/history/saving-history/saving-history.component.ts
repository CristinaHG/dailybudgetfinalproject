import { Component, OnInit } from '@angular/core';
import {User} from '../../auth/user';
import {Saving} from '../../transaction/saving/saving';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {NgForm} from '@angular/forms';
import {Expense} from '../../transaction/expense/expense';
import {DeleteComponent} from "../../delete-expense";
import {MatDialog} from "@angular/material";

@Component({
  selector: 'app-saving-history',
  templateUrl: './saving-history.component.html',
  styleUrls: ['./saving-history.component.css']
})
export class SavingHistoryComponent implements OnInit {

  public user: User;

  savings: Saving[];

  selectedDate: any;

  maxDate;

  constructor(private http: HttpClient, private router: Router, private dialog: MatDialog) { }

  ngOnInit() {
    this.http.get<Saving[]>('/api/savingHistory').subscribe(
      e => this.savings = e
    );
    this.maxDate = new Date();
    this.maxDate.setFullYear(this.maxDate.getFullYear());
    this.loadSavings();
  }

  loadSavings() {
    this.http.get<Saving[]>('/api/savingHistory').subscribe(
      e => this.savings = e
    );
  }

  onSubmit(form: NgForm) {
    console.log(form);
  }

  updateSavings() {
    this.selectedDate = undefined;
  }

  onDelete(id: number){
    const dialogRef = this.dialog.open(DeleteComponent)
    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.deleteSaving(id);
      } else {
        this.loadSavings();
      }
    })
  }

  deleteSaving(id: number) {
    this.http.post<Saving>('/api/deleteSaving', id).subscribe(
      () => {
        this.loadSavings();
      });
  }



}
