import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SavingHistoryComponent } from './saving-history.component';

describe('SavingHistoryComponent', () => {
  let component: SavingHistoryComponent;
  let fixture: ComponentFixture<SavingHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SavingHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SavingHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
