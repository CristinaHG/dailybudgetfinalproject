import { Component, OnInit } from '@angular/core';
import {Expense} from '../../transaction/expense/expense';
import {User} from '../../auth/user';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {NgForm} from '@angular/forms';
import {MatDialog} from "@angular/material";
import {DeleteComponent} from "../../delete-expense";

@Component({
  selector: 'app-expense-history',
  templateUrl: './expense-history.component.html',
  styleUrls: ['./expense-history.component.css']
})
export class ExpenseHistoryComponent implements OnInit {

  public user: User;

  expenses: Expense[];

  selectedDate: any;

  maxDate;

  constructor(private http: HttpClient, private router: Router, private dialog: MatDialog) { }

  ngOnInit() {
        this.http.get<Expense[]>('/api/expenseHistory').subscribe(
          e => this.expenses = e
        );
        this.maxDate = new Date();
        this.maxDate.setFullYear(this.maxDate.getFullYear());
        this.loadExpenses();
  }

  loadExpenses() {
    this.http.get<Expense[]>('/api/expenseHistory').subscribe(
      e => this.expenses = e
    );
  }

  onSubmit(form: NgForm) {
    console.log(form);
  }

  updateExpenses() {
    this.selectedDate = undefined;
  }

  onDelete(id: number){
    const dialogRef = this.dialog.open(DeleteComponent)
    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.deleteExpense(id);
      } else {
        this.loadExpenses();
      }
    })
  }

  deleteExpense(id: number) {
    this.http.post<Expense>('/api/deleteExpense', id).subscribe(
      () => {
        this.loadExpenses();
      });
  }

}
