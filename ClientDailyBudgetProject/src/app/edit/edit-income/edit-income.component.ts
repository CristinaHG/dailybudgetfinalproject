import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';
import {Income} from '../../transaction/income/income';

@Component({
  selector: 'app-edit-income',
  templateUrl: './edit-income.component.html',
  styleUrls: ['./edit-income.component.css']
})
export class EditIncomeComponent implements OnInit {

  editedIncome: Income;


  constructor(private http: HttpClient, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    const incomeId = this.activatedRoute.snapshot.paramMap.get('id');
    this.loadIncome(incomeId);
  }

  loadIncome(id: string) {
    this.http.get<Income>('/api/editIncome', {params: {id: id}}).subscribe(
      e => this.editedIncome = e
    );
  }

  editIncome() {
    this.http.post<Income>('/api/editIncome', this.editedIncome).subscribe(
      () => {
        this.router.navigateByUrl('history');
      });
  }

}
