import { Component, OnInit } from '@angular/core';
import {Expense} from '../../transaction/expense/expense';
import {HttpClient} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';
import {Saving} from '../../transaction/saving/saving';

@Component({
  selector: 'app-edit-saving',
  templateUrl: './edit-saving.component.html',
  styleUrls: ['./edit-saving.component.css']
})
export class EditSavingComponent implements OnInit {

  editedSaving: Saving;


  constructor(private http: HttpClient, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    const savingId = this.activatedRoute.snapshot.paramMap.get('id');
    this.loadSaving(savingId);
  }


  loadSaving(id: string) {
    this.http.get<Saving>('/api/editSaving', {params: {id: id}}).subscribe(
      e => this.editedSaving = e
    );
  }

  editSaving() {
    this.http.post<Saving>('/api/editSaving', this.editedSaving).subscribe(
      () => {
        this.router.navigateByUrl('history');
      });
  }

}
