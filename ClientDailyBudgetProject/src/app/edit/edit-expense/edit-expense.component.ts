import { Component, OnInit } from '@angular/core';
import {Expense} from '../../transaction/expense/expense';
import {HttpClient} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-edit-expense',
  templateUrl: './edit-expense.component.html',
  styleUrls: ['./edit-expense.component.css']
})
export class EditExpenseComponent implements OnInit {

  editedExpense: Expense;


  constructor(private http: HttpClient, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    const expenseId = this.activatedRoute.snapshot.paramMap.get('id');
    this.loadExpense(expenseId);
  }


  loadExpense(id: string) {
    this.http.get<Expense>('/api/editExpense', {params: {id: id}}).subscribe(
      e => this.editedExpense = e
    );
  }

  editExpense() {
    this.http.post<Expense>('/api/editExpense', this.editedExpense).subscribe(
      () => {
        this.router.navigateByUrl('history');
      });
  }

}
