export interface Budget {

  total: number;
  daysUntilMonthEnd: number;
  dailyBudget: number;
  currentDailyBudget: number;
  saving: number;

}
