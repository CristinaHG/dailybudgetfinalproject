import { Component, OnInit } from '@angular/core';
import {Budget} from './budget';
import {Income} from '../transaction/income/income';
import {HttpClient} from '@angular/common/http';
import {Expense} from '../transaction/expense/expense';
import {Router} from '@angular/router';


@Component({
  selector: 'app-budget',
  templateUrl: './budget.component.html',
  styleUrls: ['./budget.component.css']
})
export class BudgetComponent implements OnInit {

  budget: Budget = {
    total: 0,
    daysUntilMonthEnd: 0,
    dailyBudget: 0,
    currentDailyBudget: 0,
    saving: 0,
  };

  presentMonth: String;


  constructor(private http: HttpClient, private router: Router) { }


  ngOnInit() {

    const requestObservable = this.http.get<Budget>('/api/budget');
    requestObservable.subscribe(result => {
      this.budget = result;
    });
    let today = new Date().getMonth();
    this.presentMonth = this.getMonthName(today);
  }

  goToIncome() {
    this.router.navigateByUrl('income');
  }

  goToExpense() {
    this.router.navigateByUrl('expense');
  }

  getMonthName(n: number){
    let month = [];
    month[0] = "January";
    month[1] = "February";
    month[2] = "March";
    month[3] = "April";
    month[4] = "May";
    month[5] = "June";
    month[6] = "July";
    month[7] = "August";
    month[8] = "September";
    month[9] = "October";
    month[10] = "November";
    month[11] = "December";
    return month[n];
  }

}
