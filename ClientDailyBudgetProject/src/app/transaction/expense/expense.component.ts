import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {FormControl, Validators} from '@angular/forms';
import {Expense} from './expense';
import {User} from '../../auth/user';
import {Router} from '@angular/router';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-expense',
  templateUrl: './expense.component.html',
  styleUrls: ['./expense.component.css']
})
export class ExpenseComponent implements OnInit {

  public user: User;

  newExpense: Expense = {
    name: '',
    amount: undefined,
    monthly: false,
    id: 0,
    date: '',
  };

  expenses: Expense[];

  negativeAmount = false;

  constructor(private http: HttpClient, private router: Router) {
  }

  ngOnInit() {
  }

  onSubmit(form: NgForm) {
    console.log(form);
  }

  createExpense() {

    if(this.newExpense.amount < 0){
      this.negativeAmount = true;
      this.newExpense.amount = undefined;
    } else {
      this.http.post<Expense[]>('/api/expense', this.newExpense).subscribe(
        () => {
          this.router.navigateByUrl('budget');
        });
    }
  }


}




