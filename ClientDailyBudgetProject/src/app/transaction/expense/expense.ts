export interface Expense {
  amount: number;
  name: string;
  monthly: boolean;
  date: string;
  id: number;
}

