export interface Income {
  name: string;
  amount: number;
  monthly: boolean;
  date: string;
  id: number;
}
