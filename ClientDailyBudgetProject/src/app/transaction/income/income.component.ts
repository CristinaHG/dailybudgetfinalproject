import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import {User} from '../../auth/user';
import {Income} from './income';
import {Router} from '@angular/router';

@Component({
  selector: 'app-income',
  templateUrl: './income.component.html',
  styleUrls: ['./income.component.css']
})
export class IncomeComponent implements OnInit {

  public user: User;

  newIncome: Income = {
    name: '',
    amount: undefined,
    monthly: false,
    id: 0,
    date: '',
  };

  incomes: Income[];

  negativeAmount = false;

  constructor(private http: HttpClient, private router: Router) {
  }

  ngOnInit() {
  }

  onSubmit(form: NgForm) {
    console.log(form);
  }

  createIncome() {
    if(this.newIncome.amount < 0){
      this.negativeAmount = true;
      this.newIncome.amount = undefined;
    } else {
      this.http.post<Income>('/api/income', this.newIncome).subscribe(
        () => {
          this.router.navigateByUrl('budget');
        }
      );
    }
  }


}
