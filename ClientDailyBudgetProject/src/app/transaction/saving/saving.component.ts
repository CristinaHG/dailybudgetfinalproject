import { Component, OnInit } from '@angular/core';
import {User} from '../../auth/user';
import {Saving} from './saving';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {NgForm} from '@angular/forms';
import {Income} from '../income/income';

@Component({
  selector: 'app-saving',
  templateUrl: './saving.component.html',
  styleUrls: ['./saving.component.css']
})
export class SavingComponent implements OnInit {

  public user: User;

  newSaving: Saving = {
    name: '',
    amount: undefined,
    monthly: false,
    date: '',
    id: 0
  };

  savings: Saving [];

  negativeAmount = false;

  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit() {
  }

  onSubmit(form: NgForm) {
    console.log(form);
  }

  createSaving() {
    if(this.newSaving.amount < 0){
      this.negativeAmount = true;
      this.newSaving.amount = undefined;
    } else {
      this.http.post<Saving>('/api/saving', this.newSaving).subscribe(
        () => {
          this.router.navigateByUrl('budget');
        }
      );
    }
  }

}
