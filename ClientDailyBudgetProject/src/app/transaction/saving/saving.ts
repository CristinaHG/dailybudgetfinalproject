export interface Saving {
  amount: number;
  name: string;
  monthly: boolean;
  date: string;
  id: number;
}

