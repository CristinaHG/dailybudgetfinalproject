import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MaterialModule} from './material.module';
import { SignupComponent } from './auth/signup/signup.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { LoginComponent } from './auth/login/login.component';
import { HeaderComponent } from './navigation/header/header.component';
import { SidenavListComponent } from './navigation/sidenav-list/sidenav-list.component';
import { ExpenseComponent } from './transaction/expense/expense.component';
import { IncomeComponent } from './transaction/income/income.component';
import { BudgetComponent } from './budget/budget.component';
import { NotfoundComponent } from './notfound/notfound.component';
import { EditExpenseComponent } from './edit/edit-expense/edit-expense.component';
import {MAT_DATE_LOCALE} from '@angular/material';
import { ExpenseHistoryComponent } from './history/expense-history/expense-history.component';
import { IncomeHistoryComponent } from './history/income-history/income-history.component';
import { SavingComponent } from './transaction/saving/saving.component';
import { HistoryComponent } from './history/history.component';
import { SavingHistoryComponent } from './history/saving-history/saving-history.component';
import { EditIncomeComponent } from './edit/edit-income/edit-income.component';
import { EditSavingComponent } from './edit/edit-saving/edit-saving.component';
import {DeleteComponent} from './delete-expense';

@NgModule({
  declarations: [
    AppComponent,
    SignupComponent,
    LoginComponent,
    HeaderComponent,
    SidenavListComponent,
    ExpenseComponent,
    SidenavListComponent,
    IncomeComponent,
    BudgetComponent,
    NotfoundComponent,
    EditExpenseComponent,
    ExpenseHistoryComponent,
    IncomeHistoryComponent,
    SavingComponent,
    SavingHistoryComponent,
    EditIncomeComponent,
    SavingHistoryComponent,
    HistoryComponent,
    EditSavingComponent,
    DeleteComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule
  ],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'en-GB' },
  ],
  bootstrap: [AppComponent],
  entryComponents: [DeleteComponent]
})
export class AppModule { }
