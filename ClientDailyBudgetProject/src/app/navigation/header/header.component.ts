import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {LoginComponent} from '../../auth/login/login.component';
import {SecurityService} from '../../auth/services/security.service';
import {User} from '../../auth/user';

@Component({
  providers: [LoginComponent],
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  @Output()
  sidenavToggle = new EventEmitter<void>();

  user: User|null;

  constructor(private security: SecurityService, private loginComp: LoginComponent) {
    this.security.getCurrentUser().subscribe(u => this.user = u);
  }

  ngOnInit() {
  }

  onToggleSidenav(){
    this.sidenavToggle.emit();
  }

  logout(){
    this.loginComp.logout();
  }


}
