import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {LoginComponent} from "../../auth/login/login.component";
import {User} from "../../auth/user";
import {SecurityService} from "../../auth/services/security.service";
import {Router} from "@angular/router";

@Component({
  providers: [LoginComponent],
  selector: 'app-sidenav-list',
  templateUrl: './sidenav-list.component.html',
  styleUrls: ['./sidenav-list.component.css']
})
export class SidenavListComponent implements OnInit {

  @Output()
  closeSidenav = new EventEmitter<void>();

  user: User|null;

  constructor(private security: SecurityService, private loginComponent: LoginComponent, private router: Router) {
    this.security.getCurrentUser().subscribe(u => this.user = u);
  }

  ngOnInit() {
  }

  onClose() {
    this.closeSidenav.emit();
  }

  logout() {
    this.loginComponent.logout();
    this.router.navigateByUrl('login');
  }

}
